<?php

////// Индексный файл //////

//Запускаем сессию
session_start();

//Устанавливаем кодировку UTF-8
header('Content-Type: text/html; charset=UTF8');

//Устанавливаем ключ защиты для ограничения прямого доступа к файлам движка и конфигам
define('SAFETY_KEY', true);

//Подключаем файл с функциями
require_once("functions/functions.php");

//Подключаем файл с параметрами базы данных
require_once("config/db.php");


//Подключаем языковой файл с зависимости от настроек пользователя
if(isSet($_GET['lang']))
{
$lang = $_GET['lang'];
 
$_SESSION['lang'] = $lang;
 
setcookie('lang', $lang, time() + (3600 * 24 * 30));
}
else if(isSet($_SESSION['lang']))
{
$lang = $_SESSION['lang'];
}
else if(isSet($_COOKIE['lang']))
{
$lang = $_COOKIE['lang'];
}
else
{
$lang = 'ru';
}
 
switch ($lang) {
  case 'ru':
  $lang_file = 'ru.php';
  break;
 
  case 'en':
  $lang_file = 'en.php';
  break;
 
  default:
  $lang_file = 'ru.php';
 
}
require_once 'languages/'.$lang_file;

// Подключаем класс, отвечающий за авторизацию и создаем экземпляр класса
require_once("auth/login.php");
$login = new Login();

//Проверяем авторизирован ли пользователь
if ($login->isLoggedIn() == true) {
    // Если да - подключаем файл профиля
    include("views/profile.php");

} else {
    // Если нет - подключаем форму авторизации
    include("views/login_form.php");
}
