<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<title>Профиль пользователя</title>

<meta name="keywords" content="профиль пользователя">
<meta name="description" content="Приветствую, Вы вошли в свой профиль">		
		
<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">		
			
<!-- Библиотеки CSS -->
<link href="/views/css/style.css" rel="stylesheet"> 

<!-- Фавикон -->
<link rel="shortcut icon" href="/views/favicon.png">

</head>

<body>
<div class="form">
    
    <!-- Заголовок и приветствие -->
    <h1><?php echo PROFILE_HEADER; ?>
        <span><?php echo LOGOUT_HELLO.$_SESSION['user_name'].LOGOUT_YOU_ARE_LOGGED_IN; ?></span>
    </h1>   

<!-- Отображаем картинку пользователя, если она не загружена - файл default.gif -->
<p><?php 
if ($_SESSION['user_avatar_path'] != "./views/uploads/")
{
    echo "<img class=\"image\" src=\"".$_SESSION['user_avatar_path']."\">";
}
else
{
    echo "<img src=\"./views/uploads/default.gif\">";
}
?></p>
<!-- Отображаем информацию о пользователе -->
<p><?php echo PROFILE_LOGIN.": ".$_SESSION['user_login']; ?></p>
<p><?php echo PROFILE_NAME.": ".$_SESSION['user_name']; ?></p>
<?php if($_SESSION['user_gender'] == "male"){
	echo "<p>".PROFILE_GENDER.": ".PROFILE_MALE."</p>";
}else{
	echo "<p>".PROFILE_GENDER.": ".PROFILE_FEMALE."</p>";
}
?>
<p><?php echo PROFILE_EMAIL.": ".$_SESSION['user_email']; ?></p>
<?php echo "<h3>".PROFILE_ABOUT."</h3><p>".$_SESSION['user_message'].'</p>'; ?>

<!-- Кнопка "Выйти из аккаунта" -->
<a href="index.php?logout"><?php echo LOGOUT_LOGOUT ?></a>

</div><!-- form -->
</body>
</html>