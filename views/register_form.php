<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<title>Регистрация и авторизация</title>

<meta name="keywords" content="Регистрация, авторизация">
<meta name="description" content="Регистрация и авторизация, тестовое задание">		
		
<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">		
			
<!-- Библиотеки CSS -->
<link href="/views/css/style.css" rel="stylesheet"> 

<!-- Фавикон -->
<link rel="shortcut icon" href="/views/favicon.png">

<!-- Javascript для проверки заполнения полей username, email, password  -->
<!-- Для стандартных проверок на стороне клиента также можно использовать средства HTML5 -->
<script type="text/javascript">
function validate(){
   //Создаем переменные x, y, z, q и присваиваем им значения обязательных полей формы
   var x=document.forms["registerform"]["user_login"].value;
   var y=document.forms["registerform"]["user_email"].value;
   var z=document.forms["registerform"]["user_password_new"].value;
   var q=document.forms["registerform"]["user_password_repeat"].value;
   //Если длинна поля = 0 (оно не заполнено), выводим сообщение и предотвращаем отправку формы
   if (x.length===0){
      document.getElementById("user_login_empty").innerHTML="<?php echo ERROR_LOGIN_FIELD_IS_EMPTY; ?>";
      return false;
   }
   if (y.length===0){
      document.getElementById("user_email_empty").innerHTML="<?php echo ERROR_EMAIL_FIELD_IS_EMPTY; ?>";
      return false;
   }
   if (z.length===0){
      document.getElementById("password_empty").innerHTML="<?php echo ERROR_PASSWORD_FIELD_IS_EMPTY; ?>";
      return false;
   }
   if (q.length===0){
      document.getElementById("repeat_password_empty").innerHTML="<?php echo ERROR_PASSWORD_FIELD_IS_EMPTY; ?>";
      return false;
   }
}
</script>


</head>

<body>
    <form class="form" method="post" action="register.php" name="registerform" 
          enctype="multipart/form-data" onsubmit='return validate()'>   
    
    <!-- Заголовок -->
    <h1><?php echo REG_FORM_HEADER; ?>
        <span><?php echo REG_FORM_SUB_HEADER; ?></span>
    </h1>

    <!-- Выбор языка и кнопка входа в аккаунт -->
    <nav class="menu">
        <ul>
            <li><a href="?lang=ru">Русский</a></li>
            <li><a href="?lang=en">English</a></li>
            <li class="last_menu_item"><a href="index.php"><?php echo REG_FORM_LOG_IN2; ?></a></li>
        </ul>
    </nav> 

<?php
//Отображаем ошибки и сообщения сервера
if (isset($registration)) {
    if ($registration->errors) { 
        echo '<ul>';
        foreach ($registration->errors as $error) {
            echo '<li class="error">'.$error.'</li>';
        }
        echo '</ul>';
    }
    if ($registration->messages) {
        echo '<ul>';
        foreach ($registration->messages as $message) {
            echo '<li class="success">'.$message.'</li>';
        }
        echo '</ul>';
    }
}
?>

        <!-- Поля формы-->
        <label for="login_input_login"><?php echo REG_FORM_LOGIN; ?></label>
        <input id="login_input_login" class="input" type="text" name="user_login">
        <span id="user_login_empty" style="color:#F13200;"></span>
        <span class="comment"><?php echo REG_FORM_LOGIN_INFO; ?></span>
    
        <label for="login_input_password_new"><?php echo REG_FORM_PASSWORD; ?></label>
        <input id="login_input_password_new" class="input" type="password" name="user_password_new" autocomplete="off">
        <span id="password_empty" style="color:#F13200;"></span>
        
        <label for="login_input_password_repeat"><?php echo REG_FORM_REPEAT_PASSWORD; ?></label>
        <input id="login_input_password_repeat" class="input" type="password" name="user_password_repeat" autocomplete="off">
        <span id="repeat_password_empty" style="color:#F13200;"></span>
        <span class="comment"><?php echo REG_FORM_PASSWORD_INFO; ?></span>
		
		<label for="login_input_name"><?php echo REG_FORM_NAME; ?></label>
        <input id="login_input_name" class="input" type="text" name="user_name">
        <span id="user_name_empty" style="color:#F13200;"></span>
		
		<label for="login_input_email"><?php echo REG_FORM_EMAIL; ?></label>
        <input id="login_input_email" class="input" type="text" name="user_email">
        <span id="user_email_empty" style="color:#F13200;"></span>
		
		<label><?php echo REG_FORM_GENDER; ?></label>
        <input  class="radio" type="radio" name="user_gender" value="male" checked><?php echo PROFILE_MALE; ?><br>
		<input  class="radio" type="radio" name="user_gender" value="female"><?php echo PROFILE_FEMALE; ?><br>
    
        <label for="user_file"><?php echo REG_FORM_LOAD_YOUR_IMAGE; ?></label>
        <input type="hidden" name="MAX_FILE_SIZE" value="2097152">
        <input name="user_file" type="file">
        <span class="comment"><?php echo REG_FORM_FILE_INFO; ?></span>
    
        <label for="user_message"><?php echo REG_FORM_MESSAGE; ?></label>
        <textarea id="user_message" class="input" rows="5" cols="30" name="user_message"></textarea>
    
        <input class="submit" type="submit"  name="register" value="<?php echo AUTH_FORM_GO_TO_REGIST; ?>" />
    </form>
</body>
</html>


