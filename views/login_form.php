<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<title>Регистрация и авторизация</title>

<meta name="keywords" content="Регистрация, авторизация">
<meta name="description" content="Регистрация и авторизация, тестовое задание">		
		
<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">		
			
<!-- Библиотеки CSS -->
<link href="/views/css/style.css" rel="stylesheet">

<!-- Фавикон -->
<link rel="shortcut icon" href="/views/favicon.png">

<!-- Javascript для проверки заполнения полей username и password  -->
<!-- Для стандартных проверок на стороне клиента также можно использовать средства HTML5 -->
<script type="text/javascript">
function validate(){
   //Создаем переменные x и z и присваиваем им значения обязательных полей формы
   var x=document.forms["loginform"]["user_login"].value;
   var z=document.forms["loginform"]["user_password"].value;
   //Если длинна поля = 0 (оно не заполнено), выводим сообщение и предотвращаем отправку формы
   if (x.length===0){
      document.getElementById("user_login_empty").innerHTML="<?php echo ERROR_LOGIN_FIELD_IS_EMPTY; ?>";
      return false;
   }
   if (z.length===0){
      document.getElementById("password_empty").innerHTML="<?php echo ERROR_PASSWORD_FIELD_IS_EMPTY; ?>";
      return false;
   }
}
</script>
</head>

<body>
    <form class="form" method="post" action="index.php" name="loginform"
          onsubmit='return validate()'>
    <!-- Заголовок -->
        <h1><?php echo AUTH_FORM_HEADER; ?>
            <span><?php echo REG_FORM_SUB_HEADER; ?></span>
        </h1>

        <!-- Выбор языка и кнопка входа в аккаунт -->
        <nav class="menu">
            <ul>
                <li><a href="?lang=ru">Русский</a></li>
                <li><a href="?lang=en">English</a></li>
                <li class="last_menu_item"><a href="register.php"><?php echo AUTH_FORM_GO_TO_REGIST; ?></a></li>
            </ul>
        </nav> 

<?php
//Отображаем ошибки и сообщения сервера
if (isset($login)) {
    if ($login->errors) {
        echo '<ul>';
        foreach ($login->errors as $error) {
            echo '<li class="error">'.$error.'</li>';
        }
        echo '</ul>';
    }
    if ($login->messages) {
        echo '<ul>';
        foreach ($login->messages as $message) {
            echo '<li class="success">'.$message.'</li>';
        }
        echo '</ul>';
    }
}
?>
        <!-- Поля формы-->
        <label for="login_input_login"><?php echo AUTH_FORM_LOGIN; ?></label>
        <input id="login_input_login" class="input" type="text" name="user_login">
        <span id="user_login_empty" style="color:#F13200;"></span>

        <label for="login_input_password"><?php echo AUTH_FORM_PASSWORD; ?></label>
        <input id="login_input_password" class="input" type="password" name="user_password" autocomplete="off">
        <span id="password_empty" style="color:#F13200;"></span>

        <input class="submit" type="submit"  name="login" value="<?php echo REG_FORM_LOG_IN; ?>">

    </form>
</body>
</html>
