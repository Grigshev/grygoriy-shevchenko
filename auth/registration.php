<?php
////// Класс регистрации нового пользователя //////

//Ключ защиты
 if(!defined('SAFETY_KEY'))
 {
     header("HTTP/1.1 404 Not Found");
     exit(file_get_contents('../views/404.html'));
 }
 
class Registration
{
    // Инициализируем переменную  - подключение к базе данных
    private $db_connection = null;
    
    // Инициализируем переменную  - массив сообщений об ошибках
    public $errors = array();
    
    // Инициализируем переменную  - массив сообщений об успешной отработке фунций
    public $messages = array();
    
    // Максимально допустимый размер загружаемого файла - 2Мб
    public $maxFileSizeInBytes = 2097152;
    
    // Разрешенные расширения файлов для загрузки
    public $allowFileExtension = ['jpg', 'png', 'jpeg', 'gif'];

    // Папка, в которую будут загружаться файлы
    public $uploadDir = "./views/uploads/";

    // При создании класса автоматически запускаем функцию registerNewUser()
    public function __construct()
    {
        if (isset($_POST["register"])) {
            $this->registerNewUser();
        }
    }

    // Функция регистрации нового пользователя.
    private function registerNewUser()
    {
        // Если пользователь загружает файл, проверяем допустимый ли размер файла и его формат
        if (!empty($_FILES['user_file']['name'])){
            if(!in_array(pathinfo($_FILES['user_file']['name'], PATHINFO_EXTENSION), $this->allowFileExtension )) {
                $this->errors[] = ERROR_DISALLOWED_FILE_EXTENTION;
            }
            if(filesize($_FILES['user_file']['tmp_name']) > $this->maxFileSizeInBytes){
                $this->errors[] = ERROR_MAX_FILE_SIZE;
            }
        // Проверяем корректность заполнения полей
        }
        if (empty($_POST['user_login'])) {
            $this->errors[] = ERROR_LOGIN_FIELD_IS_EMPTY;
        } if (empty($_POST['user_password_new']) || empty($_POST['user_password_repeat'])) {
            $this->errors[] = ERROR_PASSWORD_FIELD_IS_EMPTY;
        } if ($_POST['user_password_new'] !== $_POST['user_password_repeat']) {
            $this->errors[] = ERROR_PASSWORDS_ARE_NOT_EQUAL;
        } if (strlen($_POST['user_password_new']) < 6) {
            $this->errors[] = ERROR_PASSWORD_MIN_LENGTH;
        } if (strlen($_POST['user_login']) > 32 || strlen($_POST['user_login']) < 2) {
            $this->errors[] = ERROR_LOGIN_LENGTH;
        } if (!preg_match('/^[A-Za-zА-ЯЁа-яё0-9]+$/u', $_POST['user_login'])) {
            $this->errors[] = ERROR_LOGIN_SCHEME;
        } if (empty($_POST['user_email'])) {
            $this->errors[] = ERROR_EMAIL_FIELD_IS_EMPTY;
        } if (strlen($_POST['user_email']) > 64) {
            $this->errors[] = ERROR_EMAIL_LENGTH;
        } if (!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = ERROR_EMAIL_NOT_VALID;
		} if (empty($_POST['user_name'])) {
            $this->errors[] = ERROR_NAME_FIELD_IS_EMPTY;
		
        
        // Если пользователь заполнил все обязательные поля и данные корректны
        // Далее по коду чистим ввод и вносим запись в базу данных
        } elseif (!empty($_POST['user_login'])
            && strlen($_POST['user_login']) <= 32
            && strlen($_POST['user_login']) >= 2
            && preg_match('/^[A-Za-zА-ЯЁа-яё0-9]+$/u', $_POST['user_login'])
            && !empty($_POST['user_email'])
            && strlen($_POST['user_email']) <= 64
            && filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)
            && !empty($_POST['user_password_new'])
            && !empty($_POST['user_password_repeat'])
            && ($_POST['user_password_new'] === $_POST['user_password_repeat'])
            && !empty($_POST['user_name'])
        ) {
            
            // Если картинка загружена и соответсвует требованиям
            // Копируем картинку в папку на сервере и отображаем сообщение об удачной загрузке
            // При неудаче возвращаем сообщение о проблемах с загрузкой
            if((!empty($_FILES['user_file']['name'])) || 
               ((!in_array(pathinfo($_FILES['user_file']['name'], PATHINFO_EXTENSION), ['jpg', 'png', 'jpeg']))
               &&(filesize($_FILES['user_file']['tmp_name']) > $this->maxFileSizeInBytes))){
            
            
                if (copy($_FILES['user_file']['tmp_name'], $this->uploadDir.basename($_FILES['user_file']['name'])))
                {
                    $this->messages[] = MESSAGE_SUCCESS_LOADING;
                }
                else
                {
                    $this->errors[] = ERROR_FILE_UPLOAD_FAILED;
                }
            }
            
            // Соединяемся с базой данных
            $this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            // Устанавливем кодирову базы данных, при неудаче возвращаем ошибку
            if (!$this->db_connection->set_charset("utf8")) {
                $this->errors[] = $this->db_connection->error;
            }

            // При удачном соединении с базой
            if (!$this->db_connection->connect_errno) {

                // Чистим текстовые поля ввода от возможных скриптов и нежелательных символов
                $user_login = $this->db_connection->real_escape_string(strip_tags($_POST['user_login'], ENT_QUOTES));
                $user_email = $this->db_connection->real_escape_string(strip_tags($_POST['user_email'], ENT_QUOTES));
                $user_message = $this->db_connection->real_escape_string(strip_tags($_POST['user_message'], ENT_QUOTES));
				$user_name = $this->db_connection->real_escape_string(strip_tags($_POST['user_name'], ENT_QUOTES));
				$user_gender = $this->db_connection->real_escape_string(strip_tags($_POST['user_gender'], ENT_QUOTES));
                
                $user_password = $_POST['user_password_new'];

                // Генерируем хэш пароля
                $user_password_hash = password_hash($user_password, PASSWORD_DEFAULT);
                
                // Вносим в переменную путь загрузки файла (для базы данных)
                // В случае отсутствия загруженного файла переменная будет равна "/views/uploads/"
                $user_avatar_path = $this->uploadDir.basename($_FILES['user_file']['name']);

                // Проверяем, есть ли в базе данных пользователь с совпадающим именем или паролем
                // Если есть, возвращаем ошибку "Логин/пароль уже занят
                $sql = "SELECT * FROM users WHERE user_login = '" . $user_login . "' OR user_email = '" . $user_email . "';";
                $query_check_user_name = $this->db_connection->query($sql);

                if ($query_check_user_name->num_rows == 1) {
                    $this->errors[] = ERROR_EMAIL_IS_ALREADY_TAKEN;
                } else {
                    // Если ошибки нет, записываем данные пользователя в базу данных
                    $sql = "INSERT INTO users (user_login, user_password_hash, user_email, user_name, user_gender, user_avatar, user_message)
                            VALUES('" . $user_login . "', '" . $user_password_hash . "', '" . $user_email . "', '" . $user_name . "', '" . $user_gender . "',
                            '" . $user_avatar_path . "', '" . $user_message . "');";
                    $query_new_user_insert = $this->db_connection->query($sql);

                    // Если запись прошла успешно
                    // Возвращаем сообщение об удачной регистрации
                    // Иначе возвращаем сообщение о неудачной регистрации
                    // Если нет соединения - об ошибке соединения с базой данных
                    // Если ошибки не выявлены, но запись не удалась - о неизвестной ошибке
                    if ($query_new_user_insert) {
                        $this->messages[] = MEGGAGE_SUCCESS_REGISTRATION;
                    } else {
                        $this->errors[] = ERROR_FAILED_REGISTRATION;
                    }
                }
            } else {
                $this->errors[] = ERROR_DATABASE_CONNECTIONS_PROBLEM;
            }
        } else {
            $this->errors[] = ERROR_UNKNOWN;
        }
    }
}
