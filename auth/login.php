<?php
////// Класс для авторизаци пользователя //////

//Ключ защиты
 if(!defined('SAFETY_KEY'))
 {
     header("HTTP/1.1 404 Not Found");
     exit(file_get_contents('../views/404.html'));
 }
 
class Login
{
    // Инициализируем переменную  - подключение к базе данных
    private $db_connection = null;
    
    // Инициализируем переменную  - массив сообщений об ошибках
    public $errors = array();
    
    // Инициализируем переменную  - массив сообщений об успешной отработке фунций
    public $messages = array();

    // При создании класса автоматически запускаем функции
    public function __construct()
    {
        //Если пользователь нажал на кнопу Logout (views/profile.php)
        if (isset($_GET["logout"])) {
            $this->logoutUser();
        }
        //Если пользователь отправил данные из формы авторизации (views/login_form.php)
        elseif (isset($_POST["login"])) {
            $this->loginUser();
        }
    }

    // Функция авторизации пользователя
    private function loginUser()
    {
        // Проверяем заполнены ли поля "Имя пользователя" и "Пароль"
        if (empty($_POST['user_login'])) {
            $this->errors[] = ERROR_LOGIN_FIELD_IS_EMPTY;
        } elseif (empty($_POST['user_password'])) {
            $this->errors[] = ERROR_PASSWORD_FIELD_IS_EMPTY;
        } elseif (!empty($_POST['user_login']) && !empty($_POST['user_password'])) {

            // Соединяемся с базой данных
            $this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            // Устанавливем кодирову базы данных, при неудаче возвращаем ошибку
            if (!$this->db_connection->set_charset("utf8")) {
                $this->errors[] = $this->db_connection->error;
            }

            // При удачном соединении с базой
            if (!$this->db_connection->connect_errno) {

                // Чистим текстовые поля ввода от возможных скриптов и нежелательных символов
                $user_login = $this->db_connection->real_escape_string($_POST['user_login']);

                // Делаем выборку всех данных о пользователе с введенным логином/e-mail 
                $sql = "SELECT *
                        FROM users
                        WHERE user_login = '" . $user_login . "' OR user_email = '" . $user_login . "';";
                $result_of_login_check = $this->db_connection->query($sql);

                // Если такой пользователь найден
                if ($result_of_login_check->num_rows == 1) {

                    // Получаем его данные в виде объекта
                    $result_row = $result_of_login_check->fetch_object();

                    // Проверяем, верный ли пароль
                    if (password_verify($_POST['user_password'], $result_row->user_password_hash)) {

                        //Если пароль верный, пишем все данные в сессию
                        $_SESSION['user_login'] = $result_row->user_login;
                        $_SESSION['user_email'] = $result_row->user_email;
                        $_SESSION['user_login_status'] = 1;
						$_SESSION['user_name'] = $result_row->user_name;
						$_SESSION['user_gender'] = $result_row->user_gender;
                        $_SESSION['user_avatar_path'] = $result_row->user_avatar;
                        $_SESSION['user_message'] = $result_row->user_message;
                    
                    // Если пароль неверный - возвращаем ошибку
                    } else {
                        $this->errors[] = ERROR_WRONG_PASSWORD;
                    }
                // Если такой пользователь не зарегистрирован - возваращаем ошибку
                } else {
                    $this->errors[] = ERROR_USER_DOES_NOT_EXIST;
                }
            // Если соединение с базой не удалось - возвращаем ошибку
            } else {
                $this->errors[] = ERROR_DATABASE_CONNECTIONS_PROBLEM;
            }
        }
    }

    // Функция выхода из авторизации
    public function logoutUser()
    {
        // Обнуляем все переменные сессии и удаляем сессию
        $_SESSION = array();
        session_destroy();
        // Возвращаем сообщение о выходе из авторизации
        $this->messages[] = MESSAGE_LOGGED_OUT;

    }

    // Функция проверки авторизации пользователя
    // Возвращаем true, если пользователь авторизирован
    // И false, если нет
    public function isLoggedIn()
    {
        if (isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] == 1) {
            return true;
        }
        return false;
    }
}
