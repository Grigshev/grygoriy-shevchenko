<?php
////// Файл подключения русского языка //////

//auth/login.php ... auth/registration.php
define("ERROR_LOGIN_FIELD_IS_EMPTY", "Поле логин не заполнено."); //reg
define("ERROR_PASSWORD_FIELD_IS_EMPTY", "Поле пароль не заполнено."); //reg
define("ERROR_DATABASE_CONNECTIONS_PROBLEM", "Не удалось соединиться с базой данных. Попробуйте войти позже.");

//auth/login.php
define("ERROR_WRONG_PASSWORD", "Неверный пароль. Проверьте правильность ввода.");
define("ERROR_USER_DOES_NOT_EXIST", "Пользователь с таким именем еще не зарегистрирован. Проверьте правильность ввода.");
define("MESSAGE_LOGGED_OUT", "Вы вышли из аккаунта.");


//auth/registation.php
define("ERROR_DISALLOWED_FILE_EXTENTION", "Загружать можно только изображения jpg, png, gif");
define("ERROR_MAX_FILE_SIZE", "Размер загружаемого файла превышает 2MB");
define("ERROR_FILE_UPLOAD_FAILED", "Загрузка файла не удалась");
define("MESSAGE_SUCCESS_LOADING", "Изображение успешно загружено на сервер");
define("ERROR_PASSWORDS_ARE_NOT_EQUAL", "Введенные пароли не совпадают. Попробуйте снова");
define("ERROR_PASSWORD_MIN_LENGTH", "В пароле должно быть не меньше 6 символов");
define("ERROR_LOGIN_LENGTH", "В логине должно быть не меньше 2 символов, но не больше 32 ");
define("ERROR_LOGIN_SCHEME", "В логине допускаються только буквы (кириллические и латинские) и цифры от 0 до 9");
define("ERROR_NAME_FIELD_IS_EMPTY", "Поле Имя не заполнено");
define("ERROR_EMAIL_FIELD_IS_EMPTY", "Поле e-mail не заполнено");
define("ERROR_EMAIL_LENGTH", "Email не должен быть длиннее 64 символов");
define("ERROR_EMAIL_NOT_VALID", "Ваш e-mail введен в неверном формате. Проверьте наличие ошибок и раскладку клавиатуры");
define("ERROR_EMAIL_IS_ALREADY_TAKEN", "Этот логин/email уже занят.");
define("ERROR_UNKNOWN", "Неизвестная ошибка.");
define("MEGGAGE_SUCCESS_REGISTRATION", "Ваш аккаунт успешно создан! Вы можете войти с Вашим логином и паролем");
define("ERROR_FAILED_REGISTRATION", "Извините, не удалось зарегистрироваться. Вернитесь на предыдущую страницу и попробуйте снова.");

// views/register_form.php
define("REG_FORM_HEADER", "Регистрация на сайте");
define("REG_FORM_SUB_HEADER", "Тестовое задание на вакансию web-разработчика");
define("REG_FORM_LOGIN", "Логин (обязательное поле)");
define("REG_FORM_LOGIN_INFO", "Допускаются кириллические/латинские буквы и цифры, длинна логина от 2 до 32 символов");
define("REG_FORM_EMAIL", "Email (обязательное поле)");
define("REG_FORM_PASSWORD", "Пароль (обязательное поле)");
define("REG_FORM_REPEAT_PASSWORD", "Повторите пароль (пароли должны совпадать)");
define("REG_FORM_PASSWORD_INFO", "Минимальная длинна пароля - 6 символов");
define("REG_FORM_NAME", "Ваше имя (обязательное поле)");
define("REG_FORM_GENDER", "Пол");
define("REG_FORM_LOG_IN", "Войти");
define("REG_FORM_LOG_IN2", "Войти в аккаунт");
define("REG_FORM_LOAD_YOUR_IMAGE", "Загрузите Ваше изображение");
define("REG_FORM_FILE_INFO", "Максимальный размер картинки - 2MB, допустимые форматы jpg, png и gif");
define("REG_FORM_MESSAGE", "Напишите немного о себе");
define("PROFILE_MESSAGE", "Коротко обо мне");

//  views/login_form.php
define("AUTH_FORM_HEADER", "Авторизация на сайте");
define("AUTH_FORM_LOGIN", "Логин");
define("AUTH_FORM_PASSWORD", "Пароль");
define("AUTH_FORM_GO_TO_REGIST", "Зарегистрироваться");

//  views/profile.php
define("LOGOUT_HELLO", "Здравствуйте,  ");
define("LOGOUT_YOU_ARE_LOGGED_IN", ". Вы вошли в Ваш аккаунт");
define("LOGOUT_LOGOUT", "Выйти");
define("PROFILE_HEADER", "Ваш Профиль");
define("PROFILE_LOGIN", "Логин");
define("PROFILE_EMAIL", "E-mail");
define("PROFILE_NAME", "Имя");
define("PROFILE_GENDER", "Пол");
define("PROFILE_MALE", "мужчина");
define("PROFILE_FEMALE", "женщина");
define("PROFILE_ABOUT", "Несколько слов о себе");



