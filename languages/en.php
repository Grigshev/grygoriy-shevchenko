<?php
////// Файл подключения английского языка //////

//auth/login.php ... auth/registration.php
define("ERROR_LOGIN_FIELD_IS_EMPTY", "Login field is empty."); //reg
define("ERROR_PASSWORD_FIELD_IS_EMPTY", "Password field is empty."); //reg
define("ERROR_DATABASE_CONNECTIONS_PROBLEM", "Database connection problem.");

//auth/login.php
define("ERROR_WRONG_PASSWORD", "Wrong password. Try again.");
define("ERROR_USER_DOES_NOT_EXIST", "This user does not exist.");
define("MESSAGE_LOGGED_OUT", "You have been logged out.");


//auth/registation.php
define("ERROR_DISALLOWED_FILE_EXTENTION", "The image must be in jpg, png or gif format.");
define("ERROR_MAX_FILE_SIZE", "File size must be less than 2MB.");
define("ERROR_FILE_UPLOAD_FAILED", "Sorry, image upload failed.");
define("MESSAGE_SUCCESS_LOADING", "Your image has been successfully uploaded.");
define("ERROR_PASSWORDS_ARE_NOT_EQUAL", "Password and password repeat are not the same");
define("ERROR_PASSWORD_MIN_LENGTH", "Password has a minimum length of 6 characters");
define("ERROR_LOGIN_LENGTH", "Login cannot be shorter than 2 or longer than 32 characters");
define("ERROR_LOGIN_SCHEME", "Login does not fit only a-Z and numbers are allowed, 2 to 32 characters");
define("ERROR_NAME_FIELD_IS_EMPTY", "Name field is empty.");
define("ERROR_EMAIL_FIELD_IS_EMPTY", "Email field is empty");
define("ERROR_EMAIL_LENGTH", "Email cannot be longer than 64 characters");
define("ERROR_EMAIL_NOT_VALID", "Your email address is not in a valid email format");
define("ERROR_EMAIL_IS_ALREADY_TAKEN", "Sorry, that username / email address is already taken.");
define("ERROR_UNKNOWN", "An unknown error occurred.");
define("MEGGAGE_SUCCESS_REGISTRATION", "Your account has been created successfully. You can now log in.");
define("ERROR_FAILED_REGISTRATION", "Sorry, your registration failed. Please go back and try again.");

// views/register_form.php
define("REG_FORM_HEADER", "Registration Form");
define("REG_FORM_SUB_HEADER", "Test task to the web-developer position");
define("REG_FORM_LOGIN", "Login (required)");
define("REG_FORM_LOGIN_INFO", "Only letters and numbers, 2 to 32 characters");
define("REG_FORM_EMAIL", "Email (required)");
define("REG_FORM_PASSWORD", "Password (required)");
define("REG_FORM_REPEAT_PASSWORD", "Repeat password (passwords must be equal)");
define("REG_FORM_PASSWORD_INFO", "Minimal password length is 6 characters");
define("REG_FORM_NAME", "Your name (required)");
define("REG_FORM_GENDER", "Your gender");
define("REG_FORM_LOG_IN", "Log in");
define("REG_FORM_LOG_IN2", "Log in");
define("REG_FORM_LOAD_YOUR_IMAGE", "Load your image");
define("REG_FORM_FILE_INFO", "Maximal size of the file - 2MB, only jpg, png and gif formats");
define("REG_FORM_MESSAGE", "Write about yourself");
define("PROFILE_MESSAGE", "About myself");


//  views/login_form.php
define("AUTH_FORM_HEADER", "Authorization Form");
define("AUTH_FORM_LOGIN", "Login");
define("AUTH_FORM_PASSWORD", "Password");
define("AUTH_FORM_GO_TO_REGIST", "Register");

//  views/profile.php
define("LOGOUT_HELLO", "Hi, ");
define("LOGOUT_YOU_ARE_LOGGED_IN", ". You are logged in");
define("LOGOUT_LOGOUT", "Logout");
define("PROFILE_HEADER", "Your Profile");
define("PROFILE_LOGIN", "Login");
define("PROFILE_NAME", "Name");
define("PROFILE_GENDER", "Gender");
define("PROFILE_MALE", "male");
define("PROFILE_FEMALE", "female");
define("PROFILE_EMAIL", "E-mail");
define("PROFILE_ABOUT", "A few words about me");



